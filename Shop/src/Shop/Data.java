package Shop;

import java.util.ArrayList;
import java.util.List;

//Singletone
public class Data {
    private static Data instance;

    private Data() {
    }

    static {
        try {
            instance = new Data();
        } catch (Exception e) {
            throw new RuntimeException("Exception occured in creating singleton instance");
        }
    }

    public static Data getInstance() {
        return instance;
    }

    public List<List<String>> datalist = new ArrayList<List<String>>();
    public List<List<String>> dataReport = new ArrayList<List<String>>();


}
