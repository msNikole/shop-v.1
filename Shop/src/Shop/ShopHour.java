package Shop;

import java.util.Random;

public class ShopHour {
    private Random rand = new Random();
    private int countOfBuyer = rand.nextInt(10);
    private SaleOfGoods saleOfGoods = new SaleOfGoods();

    public void ShopHour(boolean boolPeridMarkup, boolean boolWeekendMarkup) {
        System.out.println("count Of Buyer in hour - " + countOfBuyer);
        System.out.println("------------------------------");
        for (int i = 0; i < countOfBuyer; i++) {
            System.out.println("Sale for buyer - " + (i + 1));
            saleOfGoods.ChooseGoods(boolPeridMarkup, boolWeekendMarkup);
            System.out.println("------------------------------");
        }

    }
}
