package Shop;

public class ShopDay {

    public void ShopDay(boolean boolWeekendMarkup) {
        int openShop = 8;
        int closeShop = 21;
        int endHour = 23;
        ShopHour shopHour = new ShopHour();
        boolean boolPeridMarkup = false;

        for (int startHour = 0; startHour <= endHour; startHour++) {
            if (startHour >= 18 && startHour < 20)// В период с 18:00 до 20:00 наценка составляет 8% от закупочной цены (выходные и будние дни)
            {
                boolPeridMarkup = true;
            } else {
                boolPeridMarkup = false;
            }

            if (startHour == openShop) {
                System.out.println("           -------Shop Open-------");
            }
            if (startHour >= openShop && startHour < closeShop) {
                {
                    System.out.println("           ----Now " + startHour + " o`clock----");
                }
                shopHour.ShopHour(boolPeridMarkup, boolWeekendMarkup);
            }
            if (startHour == closeShop) {
                System.out.println("           -------Shop Close-------");
            }
        }
        ReplenishmentWarehouse warehouseReplenishment = new ReplenishmentWarehouse();
        warehouseReplenishment.ReplenishmentWarehouse();
    }

}
